# ws-server

WS-SERVER is lightweight HTTP server, which listens on `localhost:[port]/ws` for websocket messages. It receives only text messages from web socket client. If received text message contains symbol `?`, it will be converted into `!`. Converted message will be returned to client. If received message type not text, connection would be closed. 

## Supported platforms

Linux

## Test

run `go test` command in your terminal

## Build and run

run `go build` command in your terminal to compile server

run `./ws-server [parameters]` command in your terminal to run server

#### Parameters

| Command line  | Default  | Description        |
| ------------- | ---------| -------------------|
| port          | 8080     | http service port  |
| dbg           | false    | debug mode         |