package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/websocket"
)

var testAddr = "localhost:8787"

func TestServer(t *testing.T) {
	setupLog(*dbg)

	closed := make(chan bool)
	server := newServer(testAddr)
	go func() {
		server.ListenAndServe()
		closed <- true
	}()

	time.Sleep(100 * time.Millisecond) // wait server start

	ws, _, err := websocket.DefaultDialer.Dial("ws://"+testAddr+"/ws", nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	ws.Close()

	fmt.Println("shutting down server...")
	if err := server.Shutdown(context.Background()); err != nil {
		t.Errorf("server shutdown error: %v", err)
	}

	<-closed
}

type option struct {
	inputMessageType  int
	outputMessageType int
	inputMessage      string
	outputMessage     string
	inputErr          error
	outputErr         error
}

func TestHandler(t *testing.T) {
	setupLog(*dbg)

	s := httptest.NewServer(http.HandlerFunc(handler))
	defer s.Close()

	url := "ws" + strings.TrimPrefix(s.URL, "http")

	options := []option{
		{
			inputMessageType:  websocket.TextMessage,
			outputMessageType: websocket.TextMessage,
			inputMessage:      "test message",
			outputMessage:     "test message",
		},
		{
			inputMessageType:  websocket.TextMessage,
			outputMessageType: websocket.TextMessage,
			inputMessage:      "test? mess?age?",
			outputMessage:     "test! mess!age!",
		},
		{
			inputMessageType:  websocket.TextMessage,
			outputMessageType: websocket.TextMessage,
		},
		{
			inputMessageType:  websocket.BinaryMessage,
			outputMessageType: -1,
			outputErr:         errors.New("websocket: close 1003 (unsupported data): only text message is allowed"),
		},
		{
			inputMessageType:  websocket.CloseMessage,
			outputMessageType: -1,
			outputErr:         errors.New("websocket: close 1005 (no status)"),
		},
	}

	for _, option := range options {
		checkSendMessage(t, url, option)
	}
}

func checkSendMessage(t *testing.T, url string, option option) {
	ws, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer ws.Close()

	inputErr := ws.WriteMessage(option.inputMessageType, []byte(option.inputMessage))
	if isErrDiff(inputErr, option.inputErr) {
		t.Fatalf("bad write error message, expected %v, got %v", option.inputErr, inputErr)
	}

	outputMessageType, outputMessage, outputErr := ws.ReadMessage()
	if isErrDiff(outputErr, option.outputErr) {
		t.Fatalf("bad read error message, expected %v, got %v", option.outputErr, outputErr)
	}

	if outputMessageType != option.outputMessageType {
		t.Fatalf("bad message type expected %d, got %d", option.outputMessageType, outputMessageType)
	}

	if outputMessageType == websocket.CloseMessage {
		return
	}

	if string(outputMessage) != option.outputMessage {
		t.Fatalf("bad message expected %s, got %s", option.outputMessage, string(outputMessage))
	}
}

func isErrDiff(err1, err2 error) bool {
	if err1 == nil && err2 == nil {
		return false
	}

	if err1 != nil && err2 == nil || err1 == nil && err2 != nil {
		return true
	}

	if err1.Error() == err2.Error() {
		return false
	}

	return true
}
