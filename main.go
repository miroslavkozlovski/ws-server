package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/websocket"
	"github.com/hashicorp/logutils"
)

var (
	port = flag.String("port", "8080", "http service port")
	dbg  = flag.Bool("debug", false, "debug mode")
)

func main() {
	flag.Parse()

	setupLog(*dbg)

	addr := fmt.Sprintf("localhost:%s", *port)
	server := newServer(addr)
	log.Printf("[INFO] server is listening on: %s", addr)
	log.Fatal(server.ListenAndServe())
}

func newServer(addr string) *http.Server {
	mux := http.NewServeMux()
	mux.HandleFunc("/ws", handler)
	return &http.Server{
		Addr:    addr,
		Handler: mux,
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	var upgrader = websocket.Upgrader{}

	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("[ERROR] can't upgrade: %v", err)
		return
	}
	defer func() {
		if err := c.Close(); err != nil {
			log.Printf("[ERROR] can't close connection: %v\n", err)
		}
	}()

	for {
		var r io.Reader
		messageType, r, err := c.NextReader()
		if _, ok := err.(*websocket.CloseError); ok {
			break
		}
		if err != nil {
			log.Printf("[ERROR] can't get next reader: %v\n", err)
			break
		}

		log.Printf("[DEBUG] message type: %d\n", messageType)

		if messageType != websocket.TextMessage {
			log.Println("[ERROR] only text message is allowed")
			errMsg := websocket.FormatCloseMessage(websocket.CloseUnsupportedData, "only text message is allowed")
			c.WriteMessage(websocket.CloseMessage, errMsg)
			break
		}

		message, err := ioutil.ReadAll(r)
		if err != nil {
			log.Printf("[ERROR] can't read message: %v\n", err)
			break
		}

		log.Printf("[DEBUG] input message: %s\n", message)

		message = bytes.Replace(message, []byte("?"), []byte("!"), -1)

		log.Printf("[DEBUG] output message: %s\n", message)
		err = c.WriteMessage(messageType, message)
		if err != nil {
			log.Printf("[ERROR] can't write message: %v\n", err)
			break
		}
	}
}

func setupLog(dbg bool) {
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "WARN", "ERROR"},
		MinLevel: logutils.LogLevel("INFO"),
		Writer:   os.Stdout,
	}

	log.SetFlags(log.Ldate | log.Ltime)

	if dbg {
		log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
		filter.MinLevel = logutils.LogLevel("DEBUG")
	}
	log.SetOutput(filter)
}
